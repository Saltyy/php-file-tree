<?php
include("php/project_tree.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="author" content="Saltyy">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Project Tree</title>
	<link rel="stylesheet" href="css/style.css">
	<!-- add font awesome for icons -->
  	<link rel="stylesheet" type='text/css' href="css/font-awesome.css">
	<!-- makes the project tree expand/collapse -->
	<script src="js/collapse_tree.js" type="text/javascript"></script>
</head>
<body>
	<div class="main">
		<h1> Project Tree </h1>
		<?php
			echo project_tree(".", "[link]");
		?>
	</div>
</body>
</html>