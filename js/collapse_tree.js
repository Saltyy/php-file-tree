function init_project_tree() {
	if (!document.getElementsByTagName) return;
	var treeElements = document.getElementsByTagName("li");
	for (var i = 0; i < treeElements.length; i++) {
		var treeElementClass = treeElements[i].className;
		//check if tree element is a folder or not
		if (treeElementClass.indexOf("fa-folder-o") > -1) {
			var subfiles = treeElements[i].childNodes;
			for (var f = 0; f < subfiles.length; f++) {
				if (subfiles[f].tagName == "A") {
					subfiles[f].onclick = function (){
						var node = this.nextSibling;
						while (1) {
							if (node != null) {
								if (node.tagName == "UL") {
									var d = (node.style.display == "none")
									node.style.display = (d) ? "block" : "none";
									this.className = (d) ? "fa fa-folder-open-o" : "fa fa-folder-o";
									return false;
								}
								node = node.nextSibling;
							} else {
								return false;
							}
						}
						return false;
					}

					subfiles[f].className = (treeElementClass.indexOf("fa fa-folder-open-o") > -1) ? "fa fa-folder-open-o" : "fa fa-folder-o";
				}

				if (subfiles[f].tagName == "UL"){
					subfiles[f].style.display = (treeElementClass.indexOf("fa fa-folder-open-o") > -1) ? "block" : "none";
				}
			}
		}
	}
	return false;
}

window.onload = init_project_tree;