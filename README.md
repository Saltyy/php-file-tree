# PHP Project Tree

#### Description
A Project Tree built with PHP that lists all folders and files of a defined directory. By default, it uses the current directory with `"."`, but this can be changed in `index.php`.

There are two versions available:
- One with images
- One with FontAwesome icons

#### Example
[Project Tree Example](https://gitlab.saltyy.at/file-tree/)

#### Staging
_To apply changes, create merge requests and follow this process:_
- `custom-branch` -> `dev`
- `dev` -> `master`

#### Branches
- `master-fontawesome-icons`  
  Icons with FontAwesome icons (more flexible based on file type)
- `master-image-icons`  
  Icons with images provided

#### Code Snippets for Quick Adjustments

##### Change "relative-filepath" to "absolute-filepath" (with URL) in `filetree.php`
_Use the full URL instead of just the path to the file._  
_In this case, "php_project_tree" must use `$link` instead of `$filepath`._

```php
// Get current protocol
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
    $url = "https://";   
} else {
    $url = "http://";
} 
// Append the host to the URL
$url .= $_SERVER['HTTP_HOST'];   
// Append the requested resource location to the URL 
$url .= $_SERVER['REQUEST_URI'];    
// Combine current URL with file path                
$link = $url . $filepath;
```

##### PHP4 Support (scandir issue) in `filetree.php`
_PHP4 doesn't support the `scandir` function, so use the code below for compatibility with older versions._  
_To use it, replace `scandir` with `php4_scandir`, and use the check below for version support._

```php
function php4_scandir($dir) {
    $dh = opendir($dir);
    while (false !== ($filename = readdir($dh))) {
        $files[] = $filename;
    }
    sort($files);
    return $files;
}
```

A small check to determine if the `scandir` function exists. Use it together with the function above, like this:
```php
if (function_exists("scandir")) {
    $file = scandir($directory);
} else {
    $file = php4_scandir($directory);
}
```
