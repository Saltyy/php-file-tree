<?php
function project_tree($directory, $return_link, $extensions = array()){
	//generates list of all directories, sub-directories, and files in $directory
	//remove trailing slash
	if(substr($directory, -1) == "/"){
		$directory = substr($directory, 0, strlen($directory) - 1);
	}
	$code = project_tree_dir($directory, $return_link, $extensions);

	return $code;
}

//recursive function called by project_tree() to list directories/files
function project_tree_dir($directory, $return_link, $extensions = array(), $first_call = true){
	//get and sort directories/files
	$file = scandir($directory);

	//sort array (natural order case insensitve)
	natcasesort($file);

	//list directories first
	$files = $dirs = array();
	foreach($file as $this_file){
		if(is_dir("$directory/$this_file")){
			$dirs[] = $this_file;
		} else {
			$files[] = $this_file;
		}
	}
	$file = array_merge($dirs, $files);
	
	//filter unwanted extensions
	if(!empty($extensions)){
		foreach(array_keys($file) as $key){
			if(!is_dir("$directory/$file[$key]")){
				$ext = substr($file[$key], strrpos($file[$key], ".") + 1); 
				if(!in_array($ext, $extensions)){
					unset($file[$key]);
				}
			}
		}
	}
	
	if(count($file) > 2){ //use 2 instead of 0 to account for . and .. "directories"
		$project_tree = "<ul";
			if($first_call){
				$project_tree .= " class=\"tree\"";
				$first_call = false;
			}
		$project_tree .= ">";
		foreach($file as $this_file){
			if($this_file != "." && $this_file != ".."){
				if(is_dir("$directory/$this_file")){
					//define excluded folder names
					$excludedFoldersArray = array(
						".git",
						"fonts"
					);

					// check if file isnt in the "removed file" array
					if (!in_array($this_file, $excludedFoldersArray)){
						//output file
						$project_tree .= "<li class=\"directory fa fa-folder-o\"><a href=\"#\"> " . htmlspecialchars($this_file) . "</a>";
						$project_tree .= project_tree_dir("$directory/$this_file", $return_link ,$extensions, false);
						$project_tree .= "</li><br/>";
					} else {
						// Do Nothing
					}
				} else {
					//file
					//get extension of file
					$ext = substr($this_file, strrpos($this_file, ".") + 1);

					//declare icon based on extension
					if($ext == "php" || $ext == "css" || $ext == "js" || $ext == "htm" || $ext == "html" || $ext == "sql"){
						$ext = "fa fa-file-code";
					} elseif($ext == "zip" || $ext == "tar" || $ext == "7z" || $ext == "rar" || $ext == "tar.gz"){
						$ext = "fa fa-file-archive";
					} elseif($ext == "avi" || $ext == "wmv" || $ext == "mp4" || $ext == "mpg" || $ext == "mpeg"){
						$ext = "fa fa-file-video";
					} elseif($ext == "jpg" || $ext == "jpeg" || $ext == "gif" || $ext == "png"){
						$ext = "fa fa-file-image";
					} elseif($ext == "mp3" || $ext == "wav" || $ext == "ogg"){
						$ext = "fa fa-file-audio";
					} elseif($ext == "ppt" || $ext == "pptx"){
						$ext = "fa fa-file-powerpoint";
					} elseif($ext == "xls" || $ext == "xlsx"){
						$ext = "fa fa-file-excel";
					} elseif($ext == "doc" || $ext == "docx"){
						$ext = "fa fa-file-word";
					} elseif($ext == "log" || $ext == "txt"){
						$ext = "fa fa-file-text";
					} elseif($ext == "pdf"){
						$ext = "fa fa-file-pdf";
					} else {
						//default icon if none above fits
						$ext = "fa fa-file";
					}

					//get path of file
					$filepath = str_replace("[link]", "$directory/" . urlencode($this_file), $return_link);
					
					//get name of file
					$filename = basename($this_file);

					//remove "./" from the beginning of the file path		
					$filepath = substr($filepath, 2);
					
					//define excluded file names
					$excludedFilesArray = array(
						".htaccess", 
						".git", 
						"index.php"
					);

					// check if file isnt in the "removed file" array
					if (!in_array($filename, $excludedFilesArray)){
						//output file
						$project_tree .= "<li class=\"file " . $ext . "\"><a href=\"$filepath\" download=\"$filename\">" . htmlspecialchars($this_file) . "</a></li><br/>";
					} else {
						//Do Nothing
					}
				}
			}
		}
		$project_tree .= "</ul>";
	}

	//small check in case the folder is empty or not
	if(isset($project_tree)){
		return $project_tree;
	}
}